﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Age
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("A program to calculate an age!!");

            Console.WriteLine("Enter your date of birth in format dd|mm|yyyy:");

            string[] birthArray = Console.ReadLine().Split('|');
            if(birthArray.Length != 3)
            {
                Console.WriteLine("Invalid date of birth given, please retry.");
            }
            

            int day = 0;
            bool dayTrue = int.TryParse(birthArray[0], out day);
            
            int month = 0;
            bool monthTrue = int.TryParse(birthArray[1], out month);
            
            int year = 0;
            bool yearTrue = int.TryParse(birthArray[2], out year);

            string currentDate = DateTime.Now.ToString("dd-MM-yyy");

            int currentDay = Convert.ToInt32(currentDate.Split('-')[0]);
            int currentMonth = Convert.ToInt32(currentDate.Split('-')[1]);
            int currentYear = Convert.ToInt32(currentDate.Split('-')[2]);

            if (!monthTrue || !dayTrue || !yearTrue)
            {
                Console.WriteLine("Invalid birth day given");
            }
            else if(day < 1 || day > 31)
            {
                Console.WriteLine("Invalid day of birth given");
            }
            else if (month < 1 || month > 12)
            {
                Console.WriteLine("Invalid month of birth given");
            }
            else if (year < 1850 || year > currentYear)
            {
                Console.WriteLine("Invalid year of birth given");
            }
            else
            {
                int age = 0;
                if(month < currentMonth)
                {
                    age = currentYear - year;
                }
                else if(month == currentMonth)
                {
                    if(currentDay >= day)
                    {
                        age = currentYear - year;
                    }
                    else
                    {
                        age = currentYear - year - 1;
                    }
                }
                else
                {
                    age = currentYear - year - 1;
                }
                Console.WriteLine("**************************");
                Console.WriteLine("Your current age is: {0}", age);
                Console.WriteLine("**************************");
            }

        }
    }
}
